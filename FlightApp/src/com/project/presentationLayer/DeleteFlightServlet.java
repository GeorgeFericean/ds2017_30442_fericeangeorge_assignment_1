package com.project.presentationLayer;



import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.businessLogicLayer.FlightService;
import com.project.businessLogicLayer.FlightServiceImpl;
import com.project.exceptions.EntityDoesNotExistException;

public class DeleteFlightServlet extends HttpServlet {

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 5834402887539381027L;
	FlightService flightService = null;

	public DeleteFlightServlet() {
		flightService = new FlightServiceImpl();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		int id = 0;
		String url = request.getPathInfo();
		String idString = url.substring(1, url.length());
		id = Integer.parseInt(idString);

		try {
			flightService.delete(id);
			response.sendRedirect("/FlightApp/admin");
			return;
		} catch (EntityDoesNotExistException e) {
			response.sendRedirect("/FlightApp/admin");
			return;

		}

	}

}
