package com.project.presentationLayer;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.project.businessLogicLayer.FlightService;
import com.project.businessLogicLayer.FlightServiceImpl;
import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;
import com.project.entities.Flight;
import com.project.entities.User;
import com.project.exceptions.EntityDoesNotExistException;

public class UserServlet extends HttpServlet{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 4939650706400321934L;
	private UserService userService;
	private FlightService flightService;

	public UserServlet() {
		super();
		userService = new UserServiceImpl();
		flightService = new FlightServiceImpl();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession(false);
		if(session == null)
		{
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Object o = session.getAttribute("currentUsername");
		if(o == null)
		{
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String username = o.toString();
		if (username.equals("")) {
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		User user = null;
		try {
			user = userService.findByUsername(username);
		} catch (EntityDoesNotExistException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (user == null || !user.getRole().equals("user")) {
			request.setAttribute("msg", "You must be logged in as client to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		List<Flight> flights = flightService.findAll();
		
		request.setAttribute("flights", flights);

		try {
			request.getRequestDispatcher("/jsp/user.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
