package com.project.presentationLayer;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;


public class LogOutServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7272969831867569963L;
	UserService userService = null;
	
	public LogOutServlet()
	{
		userService = new UserServiceImpl();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession(false);
		 session.removeAttribute("currentUsername");
		session.invalidate();
		try {
			response.sendRedirect("/FlightApp/login");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
