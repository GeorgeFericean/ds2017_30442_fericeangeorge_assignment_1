package com.project.presentationLayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.project.businessLogicLayer.CityService;
import com.project.businessLogicLayer.CityServiceImpl;
import com.project.businessLogicLayer.FlightService;
import com.project.businessLogicLayer.FlightServiceImpl;
import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;
import com.project.entities.Flight;
import com.project.entities.User;
import com.project.exceptions.EntityDoesNotExistException;

public class LocalTimeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6109027018730406273L;
	FlightService flightService;
	UserService userService;
	CityService cityService;

	public LocalTimeServlet() {
		cityService = new CityServiceImpl();
		flightService = new FlightServiceImpl();
		userService = new UserServiceImpl();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(false);
		if (session == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");
			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Object o = session.getAttribute("currentUsername");
		if (o == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String username = o.toString();
		if (username.equals("")) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		User user = null;
		try {
			user = userService.findByUsername(username);
		} catch (EntityDoesNotExistException e1) {
			e1.printStackTrace();
		}

		if (user == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		 
		 
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/localTimeFlights.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
		List <Flight> returnedFlights = null;
		try {
			returnedFlights = flightService.findByFlightNumber(flightNumber);
		} catch (Exception e) {
			
			request.setAttribute("msg", e.getMessage());
			try {
				request.getRequestDispatcher("/jsp/localTimeFlights.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		List<Flight> localFlights=new ArrayList<Flight>();
		if (returnedFlights != null) {
			try {
				
				for(Flight flight: returnedFlights) {
					Flight localTimeFlight=flightService.getLocalTimezone(flight);
					localFlights.add(localTimeFlight);
				}
				 
				RequestDispatcher rd = request.getRequestDispatcher("/jsp/localTimeFlights.jsp");
				request.setAttribute("localFlights", localFlights);
				try {
					rd.forward(request, response);
				} catch (ServletException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			request.setAttribute("msg", "Something went wrong.");
			try {
				request.getRequestDispatcher("/jsp/localTimeFlights.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

}
