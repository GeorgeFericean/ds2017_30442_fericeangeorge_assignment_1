package com.project.presentationLayer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.project.businessLogicLayer.CityService;
import com.project.businessLogicLayer.CityServiceImpl;
import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;
import com.project.entities.City;
import com.project.entities.User;
import com.project.exceptions.EntityDoesNotExistException;

public class AddCityServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 175166383663835250L;
	CityService cityService;
	UserService userService;
	public AddCityServlet() {
		 cityService=new CityServiceImpl();
		userService = new UserServiceImpl(); 
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");
			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Object o = session.getAttribute("currentUsername");
		if (o == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String username = o.toString();
		if (username.equals("")) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		User user = null;
		try {
			user = userService.findByUsername(username);
		} catch (EntityDoesNotExistException e1) {
			e1.printStackTrace();
		}

		if (user == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!user.getRole().equals("admin")) {
			session.removeAttribute("currentUsername");
			session.invalidate();
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/create-city.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		String cityName =  request.getParameter("cityName");
		double latitude = Double.parseDouble (request.getParameter("latitude"));
		double longitude = Double.parseDouble(request.getParameter("longitude"));
		City returnedCity = null;
		try {
			returnedCity = cityService.save(cityName,latitude,longitude);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "The provided data cannot be used to instantiate a consistent object. Please complete all the fields and check your input");
			try {
				request.getRequestDispatcher("/jsp/create-city.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		if (returnedCity != null) {
			try {
				response.sendRedirect("/FlightApp/admin");
				return;

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			request.setAttribute("msg", "Something went wrong.");
			try {
				request.getRequestDispatcher("/jsp/create-city.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

}
