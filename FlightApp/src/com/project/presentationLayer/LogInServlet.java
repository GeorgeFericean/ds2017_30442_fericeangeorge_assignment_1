package com.project.presentationLayer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;
import com.project.entities.User;
import com.project.exceptions.EntityDoesNotExistException;


public class LogInServlet extends HttpServlet {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 5076828648903542393L;
	UserService userService;
	
	public LogInServlet() {
		super();
		userService=new UserServiceImpl();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/login.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username").toLowerCase();
		String password = request.getParameter("password");
		boolean logInStatus = userService.logIn(username, password);
		if (logInStatus == true) {
			User user = null;
			try {
				user = userService.findByUsername(username);
				HttpSession session = request.getSession();       
		        session.setAttribute("currentUsername",user.getUsername()); 
			} catch (EntityDoesNotExistException e) {
				request.setAttribute("msg", "The provided username does not exist");
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			}

			if (user.getRole().equals("admin")) {
				response.sendRedirect("/FlightApp/admin");
				return;
			} else if (user.getRole().equals("user")) {
				response.sendRedirect("/FlightApp/user");
				return;
			}

		} else {
			
			request.setAttribute("msg", "Invalid password");
			request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
			return;
		}

	}
}
