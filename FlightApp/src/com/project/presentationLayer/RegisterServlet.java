package com.project.presentationLayer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;
import com.project.entities.User;
import com.project.exceptions.ExistingEntityException;
import com.project.exceptions.FieldsMandatoryException;


public class RegisterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7908564807968267869L;
	
	UserService userService;
	
	public RegisterServlet()
	{
		userService = new UserServiceImpl();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response){
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/register.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response){
		
		String fullname = request.getParameter("fullname");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
 
		
		if(!password.equals(confirmPassword))
		{
			request.setAttribute("msg", "Passwords do not match.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		 
		User user = null;
		try {
			user = userService.create(username, password, fullname);
		} catch (ExistingEntityException e) {
			request.setAttribute("msg", "Username already exists.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 
		} catch (FieldsMandatoryException e) {
			request.setAttribute("msg", "All fields are mandatory.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if(user == null)
		{
			request.setAttribute("msg", "Something went wrong. Please try again.");
			try {
				request.getRequestDispatcher("/jsp/register.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		try {
			response.sendRedirect("/FlightApp/login");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}
}
