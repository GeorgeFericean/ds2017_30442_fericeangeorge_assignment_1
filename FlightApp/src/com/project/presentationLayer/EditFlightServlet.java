package com.project.presentationLayer;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.businessLogicLayer.CityService;
import com.project.businessLogicLayer.CityServiceImpl;
import com.project.businessLogicLayer.FlightService;
import com.project.businessLogicLayer.FlightServiceImpl;
import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;
import com.project.entities.City;
import com.project.entities.Flight;
import com.project.entities.User;
import com.project.exceptions.EntityDoesNotExistException;
import com.project.exceptions.IncompleteFlightFormException;

public class EditFlightServlet extends HttpServlet {

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 6136047510882518259L;
	FlightService flightService = null;
	UserService userService;
	CityService cityService;

	public EditFlightServlet() {
		flightService = new FlightServiceImpl();
		userService = new UserServiceImpl();
		cityService = new CityServiceImpl();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		String url = request.getPathInfo();
		System.out.println(url);
		String idString = url.substring(1, url.length());
		int id = Integer.parseInt(idString);

		request.getSession().setAttribute("flight_id", id);
		if (request.getSession() == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");
			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Object o = request.getSession().getAttribute("currentUsername");
		if (o == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String username = o.toString();
		if (username.equals("")) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		User user = null;
		try {
			user = userService.findByUsername(username);
		} catch (EntityDoesNotExistException e1) {
			e1.printStackTrace();
		}

		if (user == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!user.getRole().equals("admin")) {
			request.getSession().removeAttribute("currentUsername");
			request.getSession().invalidate();
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		List<City> cities = cityService.findAll();
		request.setAttribute("cities", cities);
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/editFlight.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		int id = Integer.parseInt(request.getSession().getAttribute("flight_id").toString());
		int flightNumber = Integer.parseInt(request.getParameter("newFlightNumber"));
		String type = request.getParameter("newAirplaneType");
		String departureCity = request.getParameter("newDepartureCity");

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		Date departureDate = null;
		try {
			departureDate = dateFormat.parse(request.getParameter("newDepartureDateTime"));
		} catch (ParseException e1) {
		}

		Timestamp departureDateTime = null;
		if (departureDate != null) {
			departureDateTime = new Timestamp(departureDate.getTime());
		}

		String arrivalCity = request.getParameter("newArrivalCity");

		Date arrivalDate = null;
		try {
			arrivalDate = dateFormat.parse(request.getParameter("newArrivalDateTime"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Timestamp arrivalDateTime = null;
		if (arrivalDate != null) {
			arrivalDateTime = new Timestamp(arrivalDate.getTime());
		}

		Flight newFlight = new Flight(flightNumber, type, departureCity, departureDateTime, arrivalCity,
				arrivalDateTime);
		try {
			try {
				flightService.update(id, newFlight);
			} catch (IncompleteFlightFormException e) {
				request.setAttribute("msg", e.getMessage());
			}
			response.sendRedirect("/FlightApp/admin");
			return;
		} catch (EntityDoesNotExistException e) {
			request.setAttribute("msg", "Inconsistent data.");

			try {
				request.getRequestDispatcher("/jsp/editFlight.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
	}

}
