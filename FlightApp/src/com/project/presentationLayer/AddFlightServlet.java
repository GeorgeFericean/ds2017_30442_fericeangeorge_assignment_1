package com.project.presentationLayer;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.project.businessLogicLayer.CityService;
import com.project.businessLogicLayer.CityServiceImpl;
import com.project.businessLogicLayer.FlightService;
import com.project.businessLogicLayer.FlightServiceImpl;
import com.project.businessLogicLayer.UserService;
import com.project.businessLogicLayer.UserServiceImpl;
import com.project.entities.City;
import com.project.entities.Flight;
import com.project.entities.User;
import com.project.exceptions.EntityDoesNotExistException;

public class AddFlightServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 962423920167747788L;
	FlightService flightService;
	UserService userService;
	CityService cityService;

	public AddFlightServlet() {
		cityService = new CityServiceImpl();
		flightService = new FlightServiceImpl();
		userService = new UserServiceImpl();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(false);
		if (session == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");
			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Object o = session.getAttribute("currentUsername");
		if (o == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String username = o.toString();
		if (username.equals("")) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		User user = null;
		try {
			user = userService.findByUsername(username);
		} catch (EntityDoesNotExistException e1) {
			e1.printStackTrace();
		}

		if (user == null) {
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!user.getRole().equals("admin")) {
			session.removeAttribute("currentUsername");
			session.invalidate();
			request.setAttribute("msg", "You do not have the rights to view this page");

			try {
				request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<City> cities = cityService.findAll();
		request.setAttribute("cities", cities);
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/create-flight.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
		String type = request.getParameter("airplaneType");
		String departureCity = request.getParameter("departureCity");
		String arrivalCity = request.getParameter("arrivalCity");

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

		Date departureDate = null;
		try {
			departureDate = dateFormat.parse(request.getParameter("departureDateTime"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Timestamp departureDateTime = null;
		if (departureDate != null) {
			departureDateTime = new Timestamp(departureDate.getTime());
		}

		Date arrivalDate = null;
		try {
			arrivalDate = dateFormat.parse(request.getParameter("arrivalDateTime"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Timestamp arrivalDateTime = null;
		if (arrivalDate != null) {
			arrivalDateTime = new Timestamp(arrivalDate.getTime());
		}
		Flight returnedFlight = null;
		try {
			returnedFlight = flightService.save(flightNumber, type, departureCity, departureDateTime, arrivalCity,
					arrivalDateTime);
		} catch (Exception e) {
			 
			request.setAttribute("msg", "The provided data cannot be used to instantiate a consistent object. Please complete all the fields and check your input");
			try {
				request.getRequestDispatcher("/jsp/create-flight.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		if (returnedFlight != null) {
			try {
				response.sendRedirect("/FlightApp/admin");
				return;

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			request.setAttribute("msg", "Something went wrong.");
			try {
				request.getRequestDispatcher("/jsp/create-flight.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

}
