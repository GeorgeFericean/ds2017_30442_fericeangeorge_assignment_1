package com.project.exceptions;

public class ExistingEntityException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1897440508713238503L;
	
	public ExistingEntityException(String msg)
	{
		super(msg);
	}
}
