package com.project.exceptions;

public class EntityDoesNotExistException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1897440508713238503L;
	
	public EntityDoesNotExistException(String msg)
	{
		super(msg);
	}
}
