package com.project.exceptions;

public class IncompleteFlightFormException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4004037902637586128L;
	public IncompleteFlightFormException(String msg)
	{
		super(msg);
	}
}
