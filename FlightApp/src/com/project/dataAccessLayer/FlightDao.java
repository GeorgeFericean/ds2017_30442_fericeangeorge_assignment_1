package com.project.dataAccessLayer;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.project.entities.Flight;




public class FlightDao implements FlightDaoInterface<Flight,String>{
	private Session currentSession;
	private Transaction currentTransaction;
	static SessionFactory sessionFactory;
	public FlightDao(){
		
	}
	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();

	}

	private static SessionFactory getSessionFactory() {
		 if (sessionFactory == null)   
	      {  
	         Configuration configuration = new Configuration().configure();  
	         ServiceRegistryBuilder registry = new ServiceRegistryBuilder();  
	         registry.applySettings(configuration.getProperties());  
	         ServiceRegistry serviceRegistry = registry.buildServiceRegistry();  
	         sessionFactory = configuration.buildSessionFactory(serviceRegistry);             
	      }  
	      return sessionFactory;  
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public Flight save(Flight entity) {
		Integer id=null;
		try {
			id=(Integer)getCurrentSession().save(entity);
		}
		catch(Exception e) {
			this.update(entity);
		}
		return entity;
		
	}

	public boolean update(Flight entity) {
		try {
			getCurrentSession().update(entity);
			return true;
			}
			catch (Exception e) {
				return false;
			}
		
	}

	public Flight findById(int id) {
		Flight flight = (Flight) getCurrentSession().get(Flight.class, id);
		return flight;
	}

	public boolean delete(Flight entity) {
		try {
		getCurrentSession().delete(entity);
		return true;
		}
		catch  (Exception e){
			return false;
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findAll() {
		List<Flight> flights = (List<Flight>) getCurrentSession().createQuery(
				"from "+Flight.class.getName()).list();
		return flights;
	}
	@SuppressWarnings("unchecked")
	public List<Flight> findByFlightNumber(int flightNumber) {
		List<Flight> flights = (List<Flight>) getCurrentSession().createQuery(
				"from "+Flight.class.getName()+" where flightNumber = "+flightNumber).list();
		return flights;
	}

}
