package com.project.dataAccessLayer;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.project.entities.City;
import com.project.entities.Flight;


public class CityDao implements CityDaoInterface<City,String> {
	private Session currentSession;
	private Transaction currentTransaction;
	static SessionFactory sessionFactory;
	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();

	}

	private static SessionFactory getSessionFactory() {
		 if (sessionFactory == null)   
	      {  
	         Configuration configuration = new Configuration().configure();  
	         ServiceRegistryBuilder registry = new ServiceRegistryBuilder();  
	         registry.applySettings(configuration.getProperties());  
	         ServiceRegistry serviceRegistry = registry.buildServiceRegistry();  
	         sessionFactory = configuration.buildSessionFactory(serviceRegistry);             
	      }  
	      return sessionFactory;  
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}
	public City save(City entity) {
		Integer id=null;
		try {
			id=(Integer)getCurrentSession().save(entity);
		}
		catch(Exception e) {
			this.update(entity);
		}
		return entity;
		
		
	}
	public boolean update(City entity) {
		try {
			getCurrentSession().update(entity);
			return true;
			}
			catch (Exception e) {
				return false;
			}
		
	}
	public City findById(String id) {
		City city = (City) getCurrentSession().get(City.class, id);
		return city;
	}

	@SuppressWarnings("unchecked")
	public List<City> findAll() {
		List<City> cities = (List<City>) getCurrentSession().createQuery(
				"from "+City.class.getName()).list();
		return cities;
	}

	public City findByName(String name) {
		List<City> cities = (List<City>) getCurrentSession().createQuery(
				"from "+City.class.getName()).list();
		for(City city: cities) {
			if(city.getName().equalsIgnoreCase(name)) {
				return city;
			}
		}
		return null;
	}

}
