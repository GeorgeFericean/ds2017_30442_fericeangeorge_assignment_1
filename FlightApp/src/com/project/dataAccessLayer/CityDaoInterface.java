package com.project.dataAccessLayer;

import java.io.Serializable;
import java.util.List;

public interface CityDaoInterface<T, Id extends Serializable> {
	public T save(T entity);
	public T findById(Id id);
	public List<T> findAll();
	public T findByName(String name);
}
