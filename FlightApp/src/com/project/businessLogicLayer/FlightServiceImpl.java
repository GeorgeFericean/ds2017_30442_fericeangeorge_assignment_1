package com.project.businessLogicLayer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.List;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.project.dataAccessLayer.CityDao;
import com.project.dataAccessLayer.FlightDao;
import com.project.entities.City;
import com.project.entities.Flight;
import com.project.exceptions.EntityDoesNotExistException;
import com.project.exceptions.IncompleteFlightFormException;




public class FlightServiceImpl implements FlightService {
	
	private FlightDao flightDao;
	private CityDao cityDao;
	
	public FlightServiceImpl(){
		flightDao=new FlightDao();
		cityDao=new CityDao();
	}

	public Flight save(int flightNumber, String airplaneType, String departureCity,
			Timestamp departureDateTime, String arrivalCity, Timestamp arrivalDateTime) throws IncompleteFlightFormException {
		if(Integer.toString(flightNumber)==null || Integer.toString(flightNumber).equals("")
				|| airplaneType==null || airplaneType.equals("")
				|| departureCity==null || departureCity.equals("")
				|| departureDateTime==null || departureDateTime.equals("")
				|| arrivalCity==null || arrivalCity.equals("")
				|| arrivalDateTime==null || arrivalDateTime.equals("") || arrivalDateTime.getTime()<=departureDateTime.getTime()) {
			throw new IncompleteFlightFormException("The submited data does not match the requirement");
		}
		Flight flight=new Flight(flightNumber, airplaneType, departureCity,
				 departureDateTime, arrivalCity, arrivalDateTime);
		flightDao.openCurrentSessionwithTransaction();
		flightDao.save(flight);
		flightDao.closeCurrentSessionwithTransaction();
		return flight;
	}

	public Flight update(int id,Flight entity) throws EntityDoesNotExistException, IncompleteFlightFormException {
		Flight flightChanged=this.findById(id);
		
		flightChanged.setAirplaneType(entity.getAirplaneType());
		flightChanged.setArrivalCity(entity.getArrivalCity());
		flightChanged.setArrivalDateTime(entity.getArrivalDateTime());
		flightChanged.setDepartureCity(entity.getDepartureCity());
		flightChanged.setDepartureDateTime(entity.getDepartureDateTime());
		flightChanged.setFlightNumber(entity.getFlightNumber());
		if(Integer.toString(entity.getFlightNumber())==null || Integer.toString(entity.getFlightNumber()).equals("")
				|| entity.getAirplaneType()==null || entity.getAirplaneType().equals("")
				|| entity.getDepartureCity()==null || entity.getDepartureCity().equals("")
				|| entity.getDepartureDateTime()==null || entity.getDepartureDateTime().equals("")
				|| entity.getArrivalCity()==null || entity.getArrivalCity().equals("")
				|| entity.getArrivalDateTime()==null || entity.getArrivalDateTime().equals("")) {
			throw new IncompleteFlightFormException("The submited data does not match the requirement");
		}
		flightDao.openCurrentSessionwithTransaction();
		if(flightDao.update(flightChanged)) {
			flightDao.closeCurrentSessionwithTransaction();
			return flightChanged;
			}
		flightDao.closeCurrentSessionwithTransaction();
		return null;
	}

	public Flight findById(int id) {
		flightDao.openCurrentSessionwithTransaction();
		Flight flight=flightDao.findById(id);
		flightDao.closeCurrentSessionwithTransaction();
		return flight;
	}

	public List<Flight> findByFlightNumber(int flightNumber) throws IncompleteFlightFormException {
		if(Integer.toString(flightNumber)==null || Integer.toString(flightNumber).equals(""))
			throw new IncompleteFlightFormException("Incorrect flight number input");
		flightDao.openCurrentSessionwithTransaction();
		List<Flight> flights=flightDao.findByFlightNumber(flightNumber);
		flightDao.closeCurrentSessionwithTransaction();
		return flights;
	}

	public void delete(int id) throws EntityDoesNotExistException {
		flightDao.openCurrentSessionwithTransaction();
		Flight flight=flightDao.findById(id);
		try {
		flightDao.delete(flight);
		}
		catch(Exception e) {
			throw new EntityDoesNotExistException("this flight does not exist");
		}
		
		flightDao.closeCurrentSessionwithTransaction();
		
	}

	public List findAll() {
		flightDao.openCurrentSessionwithTransaction();
		List<Flight> flights=flightDao.findAll();
		flightDao.closeCurrentSessionwithTransaction();
		return flights;
	}
	private Timestamp getJsonResponseForTimestamp(URL url,Timestamp timestamp ) {
		StringBuilder jsonResponse = new StringBuilder();

		HttpURLConnection con = null;
		BufferedReader reader = null;
		try{ 
			  con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");
			if (con.getResponseCode() != 200) {
				throw new RuntimeException("HTTP GET Request Failed with Error code : " + con.getResponseCode());
			}
			reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String output = null;
			while ((output = reader.readLine()) != null)
				jsonResponse.append(output);
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				con.disconnect();
			}
		}
		String jsonResponseString = jsonResponse.toString();
		System.out.println(jsonResponseString);
		JsonObject json = new JsonParser().parse(jsonResponseString).getAsJsonObject();

		int dstOffset = Integer.parseInt(json.get("dstOffset").toString());
		int rawOffset = Integer.parseInt(json.get("rawOffset").toString());
		long localTimestampMillis = timestamp.getTime() + ((dstOffset + rawOffset) * 1000);

		Timestamp localMillies = new Timestamp(localTimestampMillis);
		return localMillies;

	}
	@Override
	public Flight getLocalTimezone(Flight entity) throws IOException {
		cityDao.openCurrentSessionwithTransaction();
		City cityArrival=cityDao.findByName(entity.getArrivalCity());
		City cityDeparture=cityDao.findByName(entity.getDepartureCity());
		cityDao.closeCurrentSessionwithTransaction();
		//+"&key=AIzaSyADcc8McmiTrUcVsbNRpn_6Gm8wXQjylXU"
		//System.out.println("departure lat:"+cityDeparture.getLatitude()+"departure long:");
		String urlDepartureString ="https://maps.googleapis.com/maps/api/timezone/json?location="+cityDeparture.getLatitude()+","+cityDeparture.getLongitude()+"&timestamp="+entity.getDepartureDateTime().getTime() ;
		urlDepartureString = urlDepartureString.substring(0,
				urlDepartureString.length() - 1);
		 
		String urlArrivalString ="https://maps.googleapis.com/maps/api/timezone/json?location="+cityArrival.getLatitude()+","+cityArrival.getLongitude()+"&timestamp="+entity.getArrivalDateTime().getTime() ;
		urlArrivalString = urlArrivalString.substring(0,
				urlArrivalString.length() - 1);
		URL urlDeparture= new URL(urlDepartureString);
		URL urlArrival =  new URL(urlArrivalString);
		Timestamp localArrivalDateTime=getJsonResponseForTimestamp(urlArrival,entity.getArrivalDateTime());
		Timestamp localDepartureDateTime=getJsonResponseForTimestamp(urlDeparture,entity.getDepartureDateTime());
		entity.setArrivalDateTime(localArrivalDateTime);
		entity.setDepartureDateTime(localDepartureDateTime);
		return entity;
	}


}
