package com.project.businessLogicLayer;
import java.util.List;

import com.project.dataAccessLayer.CityDao;
import com.project.entities.City;




public class CityServiceImpl implements CityService {
	
	private CityDao cityDao;
	public CityServiceImpl() {
		cityDao = new CityDao();
	}

	public City findByName(String name) {
		cityDao.openCurrentSessionwithTransaction();
		City city=cityDao.findByName(name);
		cityDao.closeCurrentSessionwithTransaction();
		return city;
	}

	public List<City> findAll() {
		cityDao.openCurrentSessionwithTransaction();
		List<City> cities=cityDao.findAll();
		cityDao.closeCurrentSessionwithTransaction();
		return cities;
	}

	public City findById(int id) {
		cityDao.openCurrentSessionwithTransaction();
		City city=cityDao.findById(Integer.toString(id));
		cityDao.closeCurrentSessionwithTransaction();
		return city;
	}

	public City save(String cityName, double latitude, double longitude) {
		City city=new City(cityName,latitude,longitude);
		cityDao.openCurrentSessionwithTransaction();
		cityDao.save(city);
		cityDao.closeCurrentSessionwithTransaction();
		return city;
		
	}

}
