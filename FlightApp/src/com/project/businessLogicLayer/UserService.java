package com.project.businessLogicLayer;

import java.util.List;

import com.project.entities.User;
import com.project.exceptions.EntityDoesNotExistException;
import com.project.exceptions.ExistingEntityException;
import com.project.exceptions.FieldsMandatoryException;


public interface UserService {
	User create(String username,String password, String fullName ) throws FieldsMandatoryException, ExistingEntityException;
	List<User> findAll();
	void update(User user, int id);
	User findByUserId(int id);
	User findByUsername(String username) throws EntityDoesNotExistException;
	boolean logIn(String username, String password);
}
