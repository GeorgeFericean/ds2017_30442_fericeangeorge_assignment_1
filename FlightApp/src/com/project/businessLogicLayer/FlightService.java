package com.project.businessLogicLayer;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import com.project.entities.Flight;
import com.project.exceptions.EntityDoesNotExistException;
import com.project.exceptions.IncompleteFlightFormException;



public interface FlightService {

	public Flight save(int flightNumber, String airplaneType, String departureCity,
			Timestamp departureDateTime, String arrivalCity, Timestamp arrivalDateTime) throws IncompleteFlightFormException;
	public Flight update(int id,Flight entity) throws EntityDoesNotExistException, IncompleteFlightFormException;
	public Flight findById(int id);
	public List<Flight> findByFlightNumber(int flightNumber) throws IncompleteFlightFormException;
	public void delete(int id) throws EntityDoesNotExistException;
	public List<Flight> findAll();
	public Flight getLocalTimezone(Flight entity) throws IOException;
}
