package com.project.businessLogicLayer;

import java.util.List;

import com.project.entities.City;




public interface CityService {
	public City findByName(String name);
	public List<City> findAll();
	public City findById(int id);
	public City save (String cityName, double latitude, double longitude);	
}
