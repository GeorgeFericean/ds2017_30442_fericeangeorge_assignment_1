package com.project.entities;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;
@Entity
@Table(name="flights")
public class Flight implements java.io.Serializable{

	private static final long serialVersionUID = 4166298674112382570L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int flightNumber;
	private String airplaneType;
	private String departureCity;
	private Timestamp departureDateTime;
	private String arrivalCity;
	private Timestamp arrivalDateTime;
	public Flight() {}
	public Flight(int flightNumber, String airplaneType, String departureCity,
			Timestamp departureDateTime, String arrivalCity, Timestamp arrivalDateTime) {
		super();
		this.flightNumber=flightNumber;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.departureDateTime = departureDateTime;
		this.arrivalCity = arrivalCity;
		this.arrivalDateTime = arrivalDateTime;
	}
	public int getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	public String getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	public Timestamp getDepartureDateTime() {
		return departureDateTime;
	}
	public void setDepartureDateTime(Timestamp departureDateTime) {
		this.departureDateTime = departureDateTime;
	}
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public Timestamp getArrivalDateTime() {
		return arrivalDateTime;
	}
	public void setArrivalDateTime(Timestamp arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}
	@Override
	public String toString() {
		return "Flight: " + this.flightNumber + ", airplaneType="
				+ airplaneType + ", departureCity=" + departureCity
				+ ", departureDateTime=" + departureDateTime + ", arrivalCity="
				+ arrivalCity + ", arrivalDateTime=" + arrivalDateTime + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
