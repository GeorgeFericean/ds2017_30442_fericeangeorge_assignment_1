<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>DEPARTURE CITIES</title>

<!-- Google Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100'
	rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Font Awesome -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/owl.carousel.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/responsive.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/tableStyle.css">
</head>
<body>

	<br>
	<br>
	<div id="login" align="center">
		<form name='login' action="<c:url value='/client/ctime' />"
			method='POST'>

			<span class="fontawesome-user"></span>
			<div>
				<input type="number" id="user" name="flightNumber"
					placeholder="Flight number">
			</div>

			<span class="fontawesome-user"></span>
			<div>
				<input type="text" id="user" name="arrivalCity"
					placeholder="Arrival City">
			</div>

			<a href="redirect:/DS_Assignment1_Airport/client/ctime"><input
				name="Search" type="submit" value="Search" /></a>
		</form>
	</div>

	<div class="departureCities">
		<table>
			<thead>
				<tr>
					<th>City</th>
					<th>Latitude</th>
					<th>Longitude</th>
					<th>DatabaseTime</th>
					<th>LocalTime</th>
					<th>TimeZoneId<th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><ins>${arrivalCity}</ins></td>
					<td><ins>${cityLatitude}</ins></td>
					<td><ins>${cityLongitude}</ins></td>
					<td><ins>${dbCityDateTime}</ins></td>
					<td><ins>${localCityDateTime}</ins></td>
					<td><ins>${timeZoneId}</ins></td> 
				</tr>
			</tbody>
		</table>
	</div>

	<div align="center">
		<a href="/DS_Assignment1_Airport/client"><input type="button"
			value="Back" /></a>
	</div>

	<div align="center">
		<font size="6" color="red"><br>${msg}</font>
	</div>

	<br>
	<div class="footer-top-area">
		<div class="zigzag-bottom"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-about-us">
						<h2>
							<span>Airport application</span>
						</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="copyright">
						<p>&copy; 2017 eAirportApp. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Latest jQuery form server -->
	<script src="https://code.jquery.com/jquery.min.js"></script>

	<!-- Bootstrap JS form CDN -->
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>