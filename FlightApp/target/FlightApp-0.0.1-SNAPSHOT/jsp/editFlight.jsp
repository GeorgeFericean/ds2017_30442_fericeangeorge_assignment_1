<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDIT FLIGHT</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/logIn_style.css">
</head>

<body>
	<div id="login">
		<form name='flight' action="<c:url value='/admin/edit/${id}' />"
			method='POST'>

			<div>
				<input type="number" id="user" name="newFlightNumber"
					placeholder="New flight nr">
			</div>

			<div>
    			<label>New Plane Type</label>
			    <select name="type">
						<option value="BOEING_747">BOEING_747</option>
						<option value="AIRBUS_A300">AIRBUS_A300</option>
						<option value="LOCKHEED_L1011">LOCKHEED_L1011</option>
						<option value="TRISTAR">TRISTAR</option>	
				</select> 
			</div>
			
			<div>
				<label>New Departure City</label> <input type="text" id="user"
					name="newDepartureCity" placeholder="New Departure City">
			</div>

			<div>
				<label>New Departure DateTime</label> <input type="datetime-local"
					id="user" name="newDepartureDateTime" placeholder="New Departure DateTime">
			</div>

			<div>
				<label>New Arrival City</label> <input type="text" id="user"
					name="newArrivalCity" placeholder="New Arrival City">
			</div>

			<div>
				<label>New Arrival DateTime</label> <input type="datetime-local"
					id="user" name="newArrivalDateTime" placeholder="New Arrival DateTime">
			</div>

			<input name="Edit" type="submit" value="Edit" />
			<div align="center">
				<a href="/DS_Assignment1_Airport/admin"><input type="button"
					value="Back" /></a>
			</div>

		</form>
		<div align="center">
			<font size="6" color="red"><br>${msg}</font>
		</div>
	</div>
</body>
</html>