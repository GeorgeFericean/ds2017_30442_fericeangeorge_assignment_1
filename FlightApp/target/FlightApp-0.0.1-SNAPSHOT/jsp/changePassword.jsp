<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/logIn_style.css">
</head>
<body BGCOLOR=#ffffcc>
	<div id="login">
	<form method="POST" action="/DS_Assignment1_Airport/password">

		    
		    <span class="fontawesome-lock"></span> 
			<input type="password" id="pass" name="password" placeholder="Old Password"> 
			
			<span class="fontawesome-lock"></span> 
			<input type="password" id="pass" name="newPassword" placeholder="New Password"> 
			
			<span class="fontawesome-lock"></span> 
			<input type="password" id="pass" name="newPasswordConfirmed" placeholder="Confirm New Password">
			
			<input name="ChangePassword" type="submit" value="Change Password" />
			
		</form>
		
		<div align="center"><font size="6" color="red"><br>${msg}</font></div>
	</div>
</body>
</html>