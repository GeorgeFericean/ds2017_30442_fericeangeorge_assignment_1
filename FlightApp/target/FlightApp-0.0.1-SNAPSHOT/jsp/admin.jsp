<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ADMIN</title>

<!-- Google Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100'
	rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Font Awesome -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/owl.carousel.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/responsive.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/tableStyle.css">

</head>
<body>

	<div class="header-area">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="user-menu">
						<ul>
							<li><a href="/DS_Assignment1_Airport/logOut"><i
									class="fa fa-user"></i> Log Out</a></li>
							<li><a href="/DS_Assignment1_Airport/password"><i
									class="fa fa-user"></i> Change Password</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End header area -->

	<div class="site-branding-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="logo">
						<h1>
							<span>Administrator page</span>
						</h1>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- End site branding area -->

	<div class="mainmenu-area">
		<div class="container">
			<div class="row">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="/DS_Assignment1_Airport/admin/flight">Add flight</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- End mainmenu area -->

	<div class="product-big-title-area">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="product-bit-title text-center">
						<h2>Administrator</h2>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="flights">
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Flight number</th>
					<th>Plane type</th>
					<th>Departure city</th>
					<th>Departure date-time</th>
					<th>Arrival city</th>
					<th>Arrival date-time</th>
					<th>Delete</th>
					<th>Edit</th>
				</tr>
			</thead>
			<c:forEach var="flight" items="${flights}">
				<tr>
					<td><ins>${flight.id }</ins></td>
					<td><ins>${flight.flightNumber }</ins></td>
					<td><ins>${flight.type }</ins></td>
					<td><ins>${flight.departureCity}</ins></td>
					<td><ins>${flight.departureDateTime}</ins></td>
					<td><ins>${flight.arrivalCity}</ins></td>
					<td><ins>${flight.arrivalDateTime}</ins></td>
					<td><ins>
							<a href="/DS_Assignment1_Airport/admin/delete/${flight.id }">Delete</a>
						</ins></td>
					<td><ins>
							<a href="/DS_Assignment1_Airport/admin/edit/${flight.id }">Edit</a>
						</ins></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>

	<div class="footer-top-area">
		<div class="zigzag-bottom"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-about-us">
						<h2>
							<span>Airport App</span>
						</h2>
						<p>Thank you for visiting Airport App</p>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="copyright">
						<p>&copy; 2017 eAirport. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Latest jQuery form server -->
	<script src="https://code.jquery.com/jquery.min.js"></script>

	<!-- Bootstrap JS form CDN -->
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>