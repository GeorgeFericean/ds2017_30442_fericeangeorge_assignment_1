<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register Form</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/logIn_style.css">
</head>

<body>
	<div id="login">
		<form name='signUp' action="<c:url value='/register' />" method='POST'>

			<span class="fontawesome-user"></span>
		    <input type="text" id="user" name="fullname" placeholder="Fullname">
		    
		    <span class="fontawesome-user"></span> 
			<input type="text" id="user" name="username" placeholder="Username"> 
			
			<span class="fontawesome-lock"></span>
		    <input type="password" id="user" name="password" placeholder="Password">
		    
		    <span class="fontawesome-lock"></span>
		    <input type="password" id="user" name="confirmPassword" placeholder="Confirm Password">
		    
		    
			<input name="SignUp" type="submit" value="SignUp" />
			<div align="center"><a href="/FlightApp/login"><input type="button" value="Back" /></a></div>
			
		</form>
		<div align="center"><font size="6" color="red"><br>${msg}</font></div>
	</div>
</body>
</html>