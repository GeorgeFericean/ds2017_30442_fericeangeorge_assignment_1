<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NEW FLIGHT</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/logIn_style.css">
</head>

<body>
	<div id="login">
		<form name='flight' action="<c:url value='/admin/flight' />"
			method='POST'>

			<div>
				<input type="number" id="user" name="flightNumber"
					placeholder="Flight number">
			</div>

    		<div>
    			<label>Airplane type</label>
			    <select  name="type">
						<option value="BOEING_747">BOEING_747</option>
						<option value="AIRBUS_A300">AIRBUS_A300</option>
						<option value="LOCKHEED_L1011">LOCKHEED_L1011</option>
						<option value="TRISTAR">TRISTAR</option>	
				</select> 
			</div>
			
			<div>
				<label>Departure City</label> <input type="text" id="user"
					name="departureCity" placeholder="Departure City">
			</div>

			<div>
				<label>Departure DateTime</label> <input type="datetime-local"
					id="user" name="departureDateTime" placeholder="Departure DateTime">
			</div>

			<div>
				<label>Arrival City</label> <input type="text" id="user"
					name="arrivalCity" placeholder="Arrival City">
			</div>

			<div>
				<label>Arrival DateTime</label> <input type="datetime-local"
					id="user" name="arrivalDateTime" placeholder="Arrival DateTime">
			</div>

			<input name="Add" type="submit" value="Add" />
			<div align="center">
				<a href="/DS_Assignment1_Airport/admin"><input type="button"
					value="Back" /></a>
			</div>

		</form>
		<div align="center">
			<font size="6" color="red"><br>${msg}</font>
		</div>
	</div>
</body>
</html>