<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Form</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/logIn_style.css">
</head>

<body>
	<div id="login">
	<div align="center"><font size="3" color="red"><br>${msg}</font></div>
		<form name='login' action="<c:url value='/login' />" method='POST'>

			<span class="fontawesome-user"></span>
		    <input type="text" id="user" name="username" placeholder="Username">
		    
		    <span class="fontawesome-lock"></span> 
			<input type="password" id="pass" name="password" placeholder="Password"> 
			
			<input name="LogIn" type="submit" value="Log In" />
		    <div align="center"><a href="/FlightApp/register"><input type="button" value="Register" /></a></div>
			
		</form>
		
		

	</div>
</body>
</html>