<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Flight</title>
 <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/pageStyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/FlightApp/admin">Airport</a>
    </div>
    <ul class="nav navbar-nav">
      <li  ><a href="/FlightApp/admin/flight">Add Flight</a></li>
       <li  ><a href="/FlightApp/admin/city">Add City</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/FlightApp/logOut"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
	<div id="login">
	<div align="center">
			<font size="3" color="red"><br>${msg}</font>
		</div>
		<form class="form-horizontal" name='flight' action="<c:url value='/admin/flight' />"
			method='POST'>

			 <div class="form-group">
			 <label class="control-label col-sm-2">Flight Number</label>
				<input type="number" id="user" name="flightNumber"
				class="col-sm-6"
					placeholder="Flight number">
			</div>

    		 <div class="form-group">
    			<label class="control-label col-sm-2">Airplane type</label>
			  <input type="text" id="user" name="airplaneType"
			  class="col-sm-6"
					placeholder="Airplane Type">
			</div>
			
			<div class="form-group">
			
				<label class="control-label col-sm-2">Departure City</label>
				<select class="col-sm-6" name="departureCity">
				<c:forEach var="city" items="${cities}">
				<option><ins>${city.name}</ins></option>
				</c:forEach>
				</select>
				 
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Departure DateTime</label>
				 <input type="datetime-local"
				 class="col-sm-6"
					id="user" name="departureDateTime" placeholder="Departure DateTime">
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Arrival City</label>
				<select class="col-sm-6" name="arrivalCity">
				<c:forEach var="city" items="${cities}">
				<option><ins>${city.name}</ins></option>
				</c:forEach>
				</select>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Arrival DateTime</label>
				 <input type="datetime-local"
					class="col-sm-6" id="user" name="arrivalDateTime" placeholder="Arrival DateTime">
			</div>
			<div class="button-container">
			<input class="btn btn-default" name="Add" type="submit" value="Add" />
			<a href="/FlightApp/admin"><input class="btn btn-default" type="button"
					value="Back" /></a>
			</div>

		</form>
		
	</div>
</body>
</html>