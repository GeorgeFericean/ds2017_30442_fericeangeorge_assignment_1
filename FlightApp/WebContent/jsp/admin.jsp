<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Administrator Page</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/pageStyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Airport</a>
    </div>
    <ul class="nav navbar-nav">
      <li  ><a href="/FlightApp/admin/flight">Add Flight</a></li>
      <li  ><a href="/FlightApp/admin/city">Add City</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/FlightApp/logOut"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
	<!-- End mainmenu area -->

	<div class="product-big-title-area">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="product-bit-title text-center">
						<h2>Administrator</h2>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="flights">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Flight number</th>
					<th>Plane type</th>
					<th>Departure city</th>
					<th>Departure date-time</th>
					<th>Arrival city</th>
					<th>Arrival date-time</th>
					<th>Delete</th>
					<th>Edit</th>
				</tr>
			</thead>
			<c:forEach var="flight" items="${flights}">
				<tr>
					<td><ins>${flight.id }</ins></td>
					<td><ins>${flight.flightNumber }</ins></td>
					<td><ins>${flight.airplaneType }</ins></td>
					<td><ins>${flight.departureCity}</ins></td>
					<td><ins>${flight.departureDateTime}</ins></td>
					<td><ins>${flight.arrivalCity}</ins></td>
					<td><ins>${flight.arrivalDateTime}</ins></td>
					<td><ins>
							<a href="/FlightApp/admin/delete/${flight.id}">Delete</a>
						</ins></td>
					<td><ins>
							<a href="/FlightApp/admin/edit/${flight.id}">Edit</a>
						</ins></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>


	
</body>
</html>