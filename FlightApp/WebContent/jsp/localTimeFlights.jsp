<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Flight</title>
 <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jsp/pageStyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/FlightApp/user">Airport</a>
    </div>
    
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/FlightApp/logOut"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
	 <div>
	<div align="center">
			<font size="3" color="red"><br>${msg}</font>
		</div>
		<form class="form-horizontal" name='flight' action="<c:url value='/user/local ' />"
			method='POST'>

			 <div class="form-group">
			 <label class="control-label col-sm-2">Flight Number</label>
				<input type="number" id="user" name="flightNumber"
				class="col-sm-6"
					placeholder="Flight number">
			</div>

    		 
			<div class="button-container">
			<input class="btn btn-default" name="Find" type="submit" value="Find" />
			<a href="/FlightApp/user"><input class="btn btn-default" type="button"
					value="Back" /></a>
			</div>

		</form>
		
		<div class="flights">
		<table class="table table-bordered" >
			<thead>
				<tr>
					<th>ID</th>
					<th>Flight number</th>
					<th>Airplane Type</th>
					<th>Departure city</th>
					<th>Departure date-time</th>
					<th>Arrival city</th>
					<th>Arrival date-time</th>
				</tr>
			</thead>
			<c:forEach var="flight" items="${localFlights}">
				<tr>
					<td><ins>${flight.id }</ins></td>
					<td><ins>${flight.flightNumber }</ins></td>
					<td><ins>${flight.airplaneType }</ins></td>
					<td><ins>${flight.departureCity}</ins></td>
					<td><ins>${flight.departureDateTime}</ins></td>
					<td><ins>${flight.arrivalCity}</ins></td>
					<td><ins>${flight.arrivalDateTime}</ins></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
</body>
</html>